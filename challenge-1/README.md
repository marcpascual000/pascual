# Instructions

This first challenge only involves changing your fonts and colours - no layout or other more advanced CSS here! Check the image to see what your page should look like!

### Hints:

- You'll need to link your HTML to a new stylesheet.
- You can use the following fonts list for help: http://www.w3.org/Style/Examples/007/fonts
- Don't worry if your text doesn't wrap at the same word -- it will depend on the size of your browser window.
