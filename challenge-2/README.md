# Instructions

This second challenge will help you to re-cap your CSS text formatting skills, as well as start using margins and padding. Click on the image to see what your page should look like!

### Hints:

- The thumbnails are not very representative, but this challenge requires you to create a fixed-width layout that is centred in the browser window. Check the image for a closer view.
- To get the wrap division to stay centred, you'll need to set the left and right margins to a_t_ (you've got to fill in the blanks).